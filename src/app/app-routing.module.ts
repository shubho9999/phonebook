import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopularComponent } from './pages/popular/popular.component';
import { ContactsComponent } from './pages/contacts/contacts.component';
import {FavouriteComponent} from './pages/favourite/favourite.component';


const routes: Routes = [
  { path: '', redirectTo: 'popular', pathMatch: 'full' },
  { path: 'popular', component: PopularComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'favourite', component: FavouriteComponent },
  { path: '**', redirectTo: 'popular' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
