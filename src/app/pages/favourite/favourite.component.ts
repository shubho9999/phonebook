import { Component, OnInit } from '@angular/core';
import {ContactsService, IContact} from '../../shared/services/contacts.service';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css']
})
export class FavouriteComponent implements OnInit {

  contacts: IContact[];

  constructor(
    private contactsService: ContactsService
  ) {}

  ngOnInit() {
    this.contacts = this.contactsService.getFavouriteContacts();
  }

}
