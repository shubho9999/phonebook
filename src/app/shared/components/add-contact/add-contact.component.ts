import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.css']
})
export class AddContactComponent implements OnInit {

  nameControl = new FormControl('', [Validators.required, Validators.maxLength(50),
    Validators.pattern('^[a-zA-Z ]*$')]);
  numberControl = new FormControl('', [Validators.required, Validators.maxLength(10),
    Validators.pattern('[0-9]{10}')]);
  // loginForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddContactComponent>
  ) {
    // this.loginForm = new FormGroup({
    //   myName: this.nameControl,
    //   myNumber: this.numberControl
    // });
  }

  ngOnInit() {}

  getErrorMessageName() {
    return this.nameControl.hasError('required') ? 'Name is required' :
      this.nameControl.hasError('pattern') ? 'Only alphabet are allowed' : '';
  }

  getErrorMessageNumber() {
    return this.numberControl.hasError('required') ? 'Number is required' :
      this.numberControl.hasError('pattern') ? 'Only 10 digits are allowed' : '';
  }

}
