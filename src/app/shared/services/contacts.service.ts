import {Injectable} from '@angular/core';

export interface IContact {
  name: string;
  number: string;
  popular: boolean;
  favourite: boolean;
}

@Injectable()
export class ContactsService {

  private readonly contacts: IContact[] = [
    {name: 'Shubham', number: '9451322456', popular: true, favourite: true},
    {name: 'Ripu', number: '7745690222', popular: false, favourite: false},
    {name: 'Shagun', number: '8896897958', popular: true, favourite: true},
    {name: 'Mridu', number: '9922314490', popular: false, favourite: false}
  ];

  getContacts(): IContact[] {
    return this.contacts;
  }

  getPopularContacts(): IContact[] {
    return this.contacts.filter((contact: IContact) => {
      return (contact.popular === true);
    });
  }

  getFavouriteContacts(): IContact[] {
    return this.contacts.filter((contact: IContact) => {
      return (contact.favourite === true);
    });
  }

}
